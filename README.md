#include <iostream>
#include <math.h>

using namespace std;

int main()
{
    //Ajout des variables
	int a, b, c, d, e, r;

    //Affichage du message Bonjour
    cout <<"Bonjour"<<endl;
	cout <<"Calculer le PGCD."<<endl;
	cout <<"\n"<<endl;
	//Demande de saisie de a
	cout <<"Saisir a : "<<endl;
	cin >> a;
	//Demande de saisie de b
	cout <<"Saisir b : "<<endl;
	cin >> b;
	
	//Modulo de a par b
	r=a%b;

    //Calcule du PGCD
	while(r != 0)
	{
	c = a/b;
	d = b*c;
	r = a - d;
	a = b;
	b = r;
	}

    //Affichage du résultat du calcul du PGCD
	cout <<"Le PGCD est "+a + b + r<<endl;	

	return 0;
}
